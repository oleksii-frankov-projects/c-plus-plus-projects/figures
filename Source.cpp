#include <iostream>
#include <string>
#include <memory>
#include <vector>
#include <sstream>
#include <cmath> 
#include <iomanip>

using namespace std;

class Figure {
public:
    virtual double Area() = 0;
    virtual double Perimeter() = 0;
    virtual string Name() = 0;
};

class Triangle : public Figure {
public:
    Triangle(double a, double b, double c) : a_(a), b_(b), c_(c) {}

    double Area() override {
        float s = (float)(a_ + b_ + c_) / 2;

        return sqrt(s * (s - a_) * (s - b_) * (s - c_));
    }

    double Perimeter() override {
        return a_ + b_ + c_;
    }

    string Name() override {
        return "TRIANGLE";
    }

private:
    double a_, b_, c_;
};

class Rect : public Figure {
public:
    Rect(double width, double height) : w(width), h(height) {}

    double Area() override {
        return w * h;
    }

    double Perimeter() override {
        return (w + h) * 2;
    }

    string Name() override {
        return "RECT";
    }

private:
    double w, h;
};

class Circle : public Figure {
public:
    Circle(double radius) : r(radius) {}

    double Area() override {
        return 3.14 * r * r;
    }

    double Perimeter() override {
        return 3.14 * r * 2;
    }

    string Name() override {
        return "CIRCLE";
    }

private:
    double r;
};

shared_ptr<Figure> CreateFigure(istringstream& is) {
    string type;
    is >> type;
    if (type == "RECT") {
        double width, height;
        is >> width >> height;
        return make_shared<Rect>(width, height);
    }
    if (type == "TRIANGLE") {
        double a, b, c;
        is >> a >> b >> c;
        return make_shared<Triangle>(a, b, c);
    }
    if (type == "CIRCLE") {
        double r;
        is >> r;
        return make_shared<Circle>(r);
    }
}


int main() {
    vector<shared_ptr<Figure>> figures;

    for (string line; getline(cin, line); ) {
        istringstream is(line);

        string command;
        is >> command;
        if (command == "ADD") {
            is >> ws;
            figures.push_back(CreateFigure(is));
        }
        else if (command == "PRINT") {
            for (const auto& current_figure : figures) {
                cout << fixed << setprecision(3)
                    << current_figure->Name() << " "
                    << current_figure->Perimeter() << " "
                    << current_figure->Area() << endl;
            }
        }
    }



    return 0;
}